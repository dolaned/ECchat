import 'package:flutter/material.dart';

class Contact {
  final String id;
  final String username;
  final String imageUrl;
  final DateTime createdAt;
  Contact({
    @required this.id,
    @required this.username,
    @required this.imageUrl,
    @required this.createdAt,
  });
}

class Contacts with ChangeNotifier {
  List<Contact> _items = [
    Contact(
      id: DateTime.now().toString(),
      username: 'Griffith',
      imageUrl: 'https://loremflickr.com/320/240/dog',
      createdAt: DateTime.now(),
    ),
    Contact(
      id: DateTime.now().toString(),
      username: 'Henry',
      imageUrl: 'https://loremflickr.com/320/240/dog',
      createdAt: DateTime.now(),
    ),
    Contact(
      id: DateTime.now().toString(),
      username: 'EVhiCKVgDmooAgGWWJRGidwxAQJsv8ko9V',
      imageUrl: 'https://loremflickr.com/320/240/dog',
      createdAt: DateTime.now(),
    ),
  ];

  List<Contact> get items {
    return [..._items];
  }
}
