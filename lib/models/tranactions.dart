import 'package:flutter/foundation.dart';

enum Direction { Send, Receive }

class Transaction {
  String txId;
  double amount;
  String to;
  String from;
  Direction direction;
  DateTime createdAt;

  Transaction({
    this.txId,
    this.amount,
    this.to,
    this.from,
    this.createdAt,
    this.direction,
  });
}

class Transactions with ChangeNotifier {
  List<Transaction> _items = [
    Transaction(
      txId: 'f6862783355fbe973cc281f999d34235215e5483856cb49e42c829c85596fde3',
      amount: 5000,
      to: 'EHUzfK4rmHUQC89sTaa1ovWcG72QyMKNsh',
      from: 'EHUzfK4rmHUQC89sTaa1ovWcG72QyMKNsh',
      createdAt: DateTime.now(),
      direction: Direction.Receive,
    ),
    Transaction(
      txId: 'f6862783355fbe973cc281f999d34235215e5483856cb49e42c829c85596fde3',
      amount: 7212.93,
      to: 'EHUzfK4rmHUQC89sTaa1ovWcG72QyMKNsh',
      from: 'EHUzfK4rmHUQC89sTaa1ovWcG72QyMKNsh',
      createdAt: DateTime.now(),
      direction: Direction.Send,
    ),
    Transaction(
      txId: 'f6862783355fbe973cc281f999d34235215e5483856cb49e42c829c85596fde3',
      amount: 983.23,
      to: 'EHUzfK4rmHUQC89sTaa1ovWcG72QyMKNsh',
      from: 'EHUzfK4rmHUQC89sTaa1ovWcG72QyMKNsh',
      createdAt: DateTime.now(),
      direction: Direction.Receive,
    ),
    Transaction(
      txId: 'f6862783355fbe973cc281f999d34235215e5483856cb49e42c829c85596fde3',
      amount: 7212.93,
      to: 'EHUzfK4rmHUQC89sTaa1ovWcG72QyMKNsh',
      from: 'EHUzfK4rmHUQC89sTaa1ovWcG72QyMKNsh',
      createdAt: DateTime.now(),
      direction: Direction.Send,
    ),
  ];

  List<Transaction> get items {
    return [..._items];
  }
}
