// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dart_c_merkleblock.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DartCMerkleBlockAdapter extends TypeAdapter<DartCMerkleBlock> {
  @override
  final int typeId = 1;

  @override
  DartCMerkleBlock read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return DartCMerkleBlock(
      fields[0] as String,
      fields[1] as int,
      fields[2] as String,
      fields[3] as String,
      fields[4] as int,
      fields[5] as int,
      fields[6] as int,
      fields[7] as int,
      fields[8] as int,
      fields[9] as int,
      fields[10] as int,
      fields[11] as bool,
      (fields[12] as List)?.cast<String>(),
      fields[13] as Uint8List,
      fields[14] as Uint8List,
    );
  }

  @override
  void write(BinaryWriter writer, DartCMerkleBlock obj) {
    writer
      ..writeByte(15)
      ..writeByte(0)
      ..write(obj.blockHash)
      ..writeByte(1)
      ..write(obj.version)
      ..writeByte(2)
      ..write(obj.prevBlock)
      ..writeByte(3)
      ..write(obj.merkleRoot)
      ..writeByte(4)
      ..write(obj.timeStamp)
      ..writeByte(5)
      ..write(obj.target)
      ..writeByte(6)
      ..write(obj.nonce)
      ..writeByte(7)
      ..write(obj.totalTx)
      ..writeByte(8)
      ..write(obj.hashesCount)
      ..writeByte(9)
      ..write(obj.flagsLen)
      ..writeByte(10)
      ..write(obj.height)
      ..writeByte(11)
      ..write(obj.haveData)
      ..writeByte(12)
      ..write(obj.transactionHashes)
      ..writeByte(13)
      ..write(obj.flags)
      ..writeByte(14)
      ..write(obj.rawBytes);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DartCMerkleBlockAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
