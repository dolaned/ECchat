import '../../helpers.dart';
import 'package:hive/hive.dart';
import 'dart:typed_data';

part 'dart_c_merkleblock.g.dart';

@HiveType(typeId: 1)
class DartCMerkleBlock extends HiveObject {
  @HiveField(0)
  final String blockHash;

  @HiveField(1)
  final int version;

  @HiveField(2)
  final String prevBlock;

  @HiveField(3)
  final String merkleRoot;

  @HiveField(4)
  final int timeStamp;

  @HiveField(5)
  final int target;

  @HiveField(6)
  final int nonce;

  @HiveField(7)
  final int totalTx;

  @HiveField(8)
  final int hashesCount;

  @HiveField(9)
  final int flagsLen;

  @HiveField(10)
  final int height;

  @HiveField(11)
  final bool haveData;

  @HiveField(12)
  final List<String> transactionHashes;

  @HiveField(13)
  final Uint8List flags;

  @HiveField(14)
  final Uint8List rawBytes;

  factory DartCMerkleBlock.fromCppMessage(List message) {
    return DartCMerkleBlock(
      message[0],
      message[1],
      message[2],
      message[3],
      Helpers.parseUint32(message[4]),
      Helpers.parseUint32(message[5]),
      Helpers.parseUint32(message[6]),
      message[7],
      message[8],
      message[9],
      Helpers.parseUint32(message[10]),
      message[11],
      Helpers.parseStringList(message[12]),
      Helpers.parseUint8List(message[13]),
      Helpers.parseUint8List(message[14]),
    );
  }

  List serialize() => List.from(
        [
          blockHash,
          version,
          prevBlock,
          merkleRoot,
          Helpers.convertToUint32(timeStamp),
          Helpers.convertToUint32(target),
          Helpers.convertToUint32(nonce),
          totalTx,
          hashesCount,
          flagsLen,
          Helpers.convertToUint32(height),
          haveData,
          transactionHashes,
          flags,
          rawBytes
        ],
        growable: false,
      );

  factory DartCMerkleBlock.toCppMessage(DartCMerkleBlock block) {
    return null;
  }

  DartCMerkleBlock(
    this.blockHash,
    this.version,
    this.prevBlock,
    this.merkleRoot,
    this.timeStamp,
    this.target,
    this.nonce,
    this.totalTx,
    this.hashesCount,
    this.flagsLen,
    this.height,
    this.haveData,
    this.transactionHashes,
    this.flags,
    this.rawBytes,
  );
}
