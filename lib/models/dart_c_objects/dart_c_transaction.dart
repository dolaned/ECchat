import 'package:Ecchat/models/dart_c_objects/dart_c_txinput.dart';
import 'package:Ecchat/models/dart_c_objects/dart_c_txoutput.dart';

class DartCTransactions {
  String txHash;
  int version;
  List<DartCTxInput> inputs;
  int inCount;
  List<DartCTxOutput> outputs;
  int outCount;
  int lockTime;
  int blockHeight;
  int timestamp;
}
