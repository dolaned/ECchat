import 'dart:typed_data';

class DartCTxOutput {
  String address;
  int amount;
  Uint8List script;
  int scriptLen;
}
