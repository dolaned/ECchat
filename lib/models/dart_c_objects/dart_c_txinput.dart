import 'dart:typed_data';

class DartCTxInput {
  String txHash;
  int index;
  String address;
  int amount;
  Uint8List script;
  int scriptLen;
  Uint8List signature;
  int sigLen;
  int sequence;
}
