import 'package:flutter/material.dart';

class Conversation {
  final String id;
  final String name;
  final String imageUrl;
  final DateTime createdAt;

  Conversation({
    @required this.id,
    @required this.name,
    @required this.imageUrl,
    @required this.createdAt,
  });
}

class Conversations with ChangeNotifier {
  List<Conversation> _items = [
    Conversation(
      id: DateTime.now().toString(),
      name: 'Griffth',
      imageUrl: 'https://loremflickr.com/320/240/dog',
      createdAt: DateTime.now(),
    ),
    Conversation(
      id: DateTime.now().toString(),
      name: 'Henry',
      imageUrl: 'https://loremflickr.com/320/240/dog',
      createdAt: DateTime.now(),
    ),
    Conversation(
      id: DateTime.now().toString(),
      name: 'Griffth',
      imageUrl: 'https://loremflickr.com/320/240/dog',
      createdAt: DateTime.now(),
    ),
  ];

  List<Conversation> get items {
    return [..._items];
  }
}
