import 'dart:io';
import 'dart:ffi';
import 'dart:isolate';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:bip39/bip39.dart' as bip39;

import 'package:ffi/ffi.dart';

import 'BRCore.dart';

DynamicLibrary _lib = Platform.isAndroid
    ? DynamicLibrary.open('libcore-lib.so')
    : DynamicLibrary.process();

final brCore = BRCore(_lib);

class EccWalletManager with ChangeNotifier {
  Pointer<Wallet> wallet;
  double walletBalance = 0.0;
  String currentAddress;
  ReceivePort cConnection;
  int nativePort;

  EccWalletManager() {
    print('Wallet created');
    // set peer manager callbacks.

    // start connection between dart and c
    cConnection = ReceivePort()..listen(requestExecuteCallback);
    nativePort = cConnection.sendPort.nativePort;
    brCore.registerWalletDartPort(nativePort);
  }

  void requestExecuteCallback(dynamic message) {
    print(message);
    // try {
    //   final cppRequest = CppRequest.fromCppMessage(message);
    //   switch (cppRequest.method) {
    //     case 'saveHeadersDart':
    //       final merkBlock = DartCMerkleBlock.fromCppMessage(cppRequest.data);
    //       break;
    //   }
    // } catch (e) {
    //   print(e);
    // }
  }

  @override
  void dispose() {
    brCore.releaseWallet(this.wallet);
    super.dispose();
  }

  static void balChanged(Pointer<void> info, int balance) {
    print('Balance Changed');
    print(balance);
    print(info.toString());
  }

  static void txAdded(Pointer<void> info, Pointer<Transaction> tx) {
    print('Transaction Added');
    print(info);
  }

  static void txUpdated(Pointer<void> info, Pointer<uint256> hashes,
      int txCount, int blockheight, int timestamp) {
    print('Transaction updated');
    print(info);
  }

  setWalletCallBacks() {
    brCore.setWalletCallbacks(
      this.wallet,
    );
  }

  Future<bool> checkForWallet() async {
    final secureStore = FlutterSecureStorage();
    final phrase = await secureStore.read(key: 'mnemonic');
    return phrase != null;
  }

  Future<void> initWallet(String words, int epochTimestamp) async {
    final walletExists = await this.checkForWallet();
    if (!walletExists) {
      await setupNewWallet(words, epochTimestamp);
    }

    final secureStore = FlutterSecureStorage();
    final phrase = await secureStore.read(key: 'mnemonic');
    final epochTime =
        num.tryParse(await secureStore.read(key: 'earliestKeyTime'));
    print(phrase);
    print(epochTime);

    this.wallet = brCore.createWallet(
      Utf8.toUtf8(phrase).cast<Int8>(),
    );
    setWalletCallBacks();
  }

  Future<void> setupNewWallet(String words, int epochTime) async {
    final secureStore = new FlutterSecureStorage();
    if (words == null) {
      words = bip39.generateMnemonic();
    }

    await secureStore.write(
      key: 'mnemonic',
      value: words,
    );

    await secureStore.write(
      key: 'earliestKeyTime',
      value: epochTime.toString(),
    );
  }

  String get receiveAddress {
    // final Pointer<Uint8> address = allocate<Uint8>(count: 34 + 1);
    final Pointer<Int8> address =
        Utf8.toUtf8('EZtQfDhG4g4gTnmUov9t6byvea5QijoVaQ').cast<Int8>();
    brCore.receiveAddress(this.wallet, address.cast());
    String _address = Utf8.fromUtf8(address.cast<Utf8>());
    free(address);
    return _address;
  }

  List<Address> get allAddress {
    Pointer<Address> addr = allocate();
    int addressCount = brCore.allAddresses(wallet, addr);
    print(addressCount);

    List<Address> addressList = [];

    for (int i = 0; i < addressCount; i++) {
      final address = Utf8.fromUtf8(addr.elementAt(i).cast());
      print(address);
      addressList.add(addr.elementAt(i).ref);
    }
    free(addr);
    return addressList;
  }

  bool containsAddress(String address) {
    return brCore.containsAddress(wallet, Utf8.toUtf8(address).cast<Int8>())
        as bool;
  }

  bool addressIsUsed(String address) {
    return brCore.addressIsUsed(wallet, Utf8.toUtf8(address).cast<Int8>())
        as bool;
  }

  List get transactions {}

  int get balance {
    return brCore.balance(wallet);
  }

  int get totalSent {
    return brCore.totalSent(wallet);
  }

  int get feePerKb {
    return brCore.feePerKb(this.wallet);
  }

  set feePerKb(int value) {
    brCore.setFeePerKb(this.wallet, value);
  }

  int getFeeForTx(int amount) {
    return brCore.feeForTx(this.wallet, amount);
  }

  Transaction createTransaction(int forAmount, String toAddress) {
    Pointer<Int8> address = Utf8.toUtf8(toAddress).cast<Int8>();
    var isValid = brCore.addressIsValid(address) == 1;
    if (!isValid) {
      return null;
    }
    return brCore
        .createTransaction(this.wallet, forAmount, address)
        .elementAt(0)
        .ref;
  }

  bool transactionIsValid(Transaction tx) {
    return brCore.transactionIsValid(this.wallet, tx.addressOf) as bool;
  }

  bool transactionIsPending(Transaction tx) {
    return brCore.transactionIsPending(this.wallet, tx.addressOf) as bool;
  }

  bool transactionIsVerified(Transaction tx) {
    return brCore.transactionIsVerified(this.wallet, tx.addressOf) as bool;
  }

  int amountReceivedFromTx(Transaction tx) {
    return brCore.amountReceivedFromTx(this.wallet, tx.addressOf);
  }

  int amountSentByTx(Transaction tx) {
    return brCore.amountSentByTx(this.wallet, tx.addressOf);
  }

  int feeForTx(Transaction tx) {
    return brCore.feeForTransaction(this.wallet, tx.addressOf);
  }

  int balanceAfterTx(Transaction tx) {
    return brCore.balanceAfterTx(this.wallet, tx.addressOf);
  }

  int feeForTxSize(int size) {
    return brCore.feeForTxSize(this.wallet, size);
  }

  int get minOutputAmount {
    return brCore.minOutPutAmount(this.wallet);
  }

  int get maxOutputAmount {
    return brCore.maxOutputAmount(this.wallet);
  }
}
