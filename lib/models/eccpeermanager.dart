import 'dart:io';
import 'dart:ffi';
import 'dart:isolate';
import 'dart:typed_data';
import 'package:flutter/foundation.dart';
import 'BRCore.dart';
import 'package:ffi/ffi.dart';
import '../models/dart_c_objects/dart_c_merkleblock.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import '../services/cpprequest.dart';
import 'package:hive/hive.dart';

DynamicLibrary _lib = Platform.isAndroid
    ? DynamicLibrary.open('libcore-lib.so')
    : DynamicLibrary.process();

final brCore = BRCore(_lib);

class ECCPeerManager extends ChangeNotifier {
  // Class member
  Pointer<Wallet> wallet;
  Pointer<PeerManager> peerManager;
  ReceivePort cConnection;
  SendPort dConnection;
  int receivePort;
  int sendPort;

  // Constructor
  ECCPeerManager(Pointer<Wallet> wallet) {
    // set wallet
    this.wallet = wallet;
    initPeerManager();
  }

  Future<void> initPeerManager() async {
    final secureStore = FlutterSecureStorage();
    final epochTime =
        num.tryParse(await secureStore.read(key: 'earliestKeyTime'));
    print('earliest keyTime: $epochTime');
    // create peer manager.
    this.peerManager = brCore.createNetworkManager(
      this.wallet,
      epochTime,
    );

    // set peer manager callbacks.
    setPeerManagerCallbacks();

    // start connection between dart and c
    cConnection = ReceivePort()..listen(requestExecuteCallback);
    receivePort = cConnection.sendPort.nativePort;
    final port = brCore.registerPeerDartPort(receivePort);
    print('port no $port');
    this.initializeHeaders();
  }

  //loop over headers and add them to peer manager set
  Future<void> initializeHeaders() async {
    final headers = await Hive.openBox<DartCMerkleBlock>('merkleHeadersBox');
    for (final key in headers.keys) {
      DartCMerkleBlock header = headers.get(key);
      final Pointer<Uint8> frameData = allocate<Uint8>(
          count: header.rawBytes.length); // Allocate a pointer large enough.
      final pointerList = frameData.asTypedList(header.rawBytes
          .length); // Create a list that uses our pointer and copy in the image data.
      pointerList.setAll(0, header.rawBytes);
      brCore.addBlockToSet(this.peerManager, frameData);
      print(header.height);
      // call c code to write to set
    }
  }

  void requestExecuteCallback(dynamic message) {
    try {
      final cppRequest = CppRequest.fromCppMessage(message);
      switch (cppRequest.method) {
        case 'saveHeadersDart':
          final replace = cppRequest.data[0];
          final data = cppRequest.data[1];
          this.saveHeaders(replace, data);
          break;
        case 'saveBlocksDart':
          print('Save blocks called');
          print(cppRequest.data);
          break;
        case 'syncStarted':
          break;
      }
    } catch (e) {
      print(e);
    }
  }

  void setPeerManagerCallbacks() {
    brCore.setPeerManagerCallbacks(this.peerManager);
  }

  @override
  void dispose() {
    print('in dispose');
    brCore.peerManagerDisconnect(this.peerManager);
    brCore.releaseNetworkManager(this.peerManager);
    this.cConnection.close();
    super.dispose();
  }

  // Getters
  bool get isConnected {
    return brCore.isConnected(this.peerManager) == 1;
  }

  int get lastBlockHeight {
    final lastBlockHeight =
        peerManager != null ? brCore.lastBlockHeight(this.peerManager) : 0;
    print(lastBlockHeight);
    return lastBlockHeight;
  }

  int get lastHeaderHeight {
    return peerManager != null ? brCore.lastHeaderHeight(this.peerManager) : 0;
  }

  int get lastBlockTimestamp {
    return brCore.lastBlockTimeStamp(this.peerManager);
  }

  int get estimatedBlockHeight {
    return brCore.estimatedBlockHeight(this.peerManager);
  }

  double get syncProgress {
    return peerManager != null ? brCore.syncProgress(this.peerManager, 0) : 0.0;
  }

  int get peerCount {
    return brCore.peerCount(this.peerManager);
  }

  String get downloadPeerName {
    return Utf8.fromUtf8(
        brCore.downloadPeerName(this.peerManager).cast<Utf8>());
  }

  int get relayCount {}

  // Methods

  void publishTx(
    Pointer<Transaction> tx,
  ) {
    // brCore.publishTx(this.peerManager, tx, info, callback);
  }

  void connect() {
    brCore.peerManagerConnect(this.peerManager);
    // cConnection.sendPort.send('yew');
  }

  void disconnect() {
    brCore.peerManagerDisconnect(this.peerManager);
  }

  void rescan() {
    brCore.rescan(this.peerManager);
  }

  void setFixedPeer(int address, int port) {
    brCore.setFixedPeer(this.peerManager, address, port);
  }

  static void txUpdated(Pointer<void> info) {
    print('Transaction updated');
    print(info);
  }

  static void syncStarted(Pointer<void> info) {
    print('Sync Started');
    print(info);
    print('pointer address to peer manager above');
  }

  static void syncStopped(Pointer<void> info, int i) {
    print('Sync stopped');
    // print(info);
    // print(i);
  }

  Future<void> saveHeaders(bool replace, dynamic data) async {
    print(data);
    final merkBlock = DartCMerkleBlock.fromCppMessage(data);
    var box = await Hive.openBox<DartCMerkleBlock>('merkleHeadersBox');

    box.put(merkBlock.height, merkBlock);

    print('serialized block');
    print(merkBlock.serialize());
    print('\n');
  }

  static void saveBlocks(Pointer<void> info, int replace,
      Pointer<Pointer<MerkleBlock>> blocks, int blocksCount) {
    print('save blocks');
    // print('replace $replace');
    // print('blocks $blocks');
    // print('blocks count $blocksCount');
  }

  static void savePeers(
      Pointer<void> info, int replace, Pointer<Peer> peers, int peersCount) {
    print('save peers');
    print('replace $replace');
    print('peers $peers');
    print('peerscount $peersCount');
  }

  static int networkIsReachable(Pointer<void> info) {
    print('network reachable');
    return 1;
  }
}
