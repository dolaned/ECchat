import 'package:flutter/material.dart';

class Message {
  final String id;
  final String content;
  final String ownerId;
  final DateTime createdAt;

  Message({
    @required this.id,
    @required this.content,
    @required this.ownerId,
    @required this.createdAt,
  });
}
