import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class SeedPhrase extends StatefulWidget {
  final String phrases;
  final Function createWallet;

  SeedPhrase(this.phrases, this.createWallet);

  @override
  _SeedPhraseState createState() => _SeedPhraseState();
}

class _SeedPhraseState extends State<SeedPhrase> {
  int wordCount;

  void incrementWords(double pos) {
    setState(() {
      wordCount = pos.floor();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 320,
        color: Theme.of(context).primaryColor,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Text(
                    'Seed Phrases',
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    'Please Write down these seed phrases in order to recover your wallet!!',
                    style: TextStyle(
                      color: Theme.of(context).errorColor,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
            CarouselSlider(
              options: CarouselOptions(
                height: 200.0,
                initialPage: 0,
                enableInfiniteScroll: false,
                onScrolled: incrementWords,
              ),
              items: widget.phrases.split(' ').map((word) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              word,
                              style: TextStyle(fontSize: 32.0),
                            ),
                          ],
                        ));
                  },
                );
              }).toList(),
            ),
            if (this.wordCount == 11)
              RaisedButton(
                padding: EdgeInsets.all(15),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(9),
                ),
                color: Theme.of(context).accentColor,
                onPressed: () {
                  widget.createWallet(context);
                },
                child: Text('Continue'),
              )
          ],
        ));
  }
}
