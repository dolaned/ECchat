import 'package:Ecchat/widgets/qr_scanner.dart';
import 'package:flutter/material.dart';
import 'package:bip21/bip21.dart';
import 'package:provider/provider.dart';
import '../../models/eccwalletmanager.dart';

class WalletSend extends StatefulWidget {
  static const routeName = '/wallet-send';
  @override
  _WalletSendState createState() => _WalletSendState();
}

class _WalletSendState extends State<WalletSend> {
  FocusNode _toFocusnode = FocusNode();
  TextEditingController _toController = TextEditingController();

  FocusNode _amountFocusNode = FocusNode();
  TextEditingController _amountController = TextEditingController();

  FocusNode _memoFocusNode = FocusNode();
  TextEditingController _memoController = TextEditingController();

  final GlobalKey qrKey = GlobalKey(debugLabel: 'send');

  int _getfeeForTx(BuildContext ctx, int amount) {
    // List<Address> addresses =
    //     Provider.of<EccWalletManager>(ctx, listen: false).allAddress;
    // print(addresses);
    // addresses.map((e) {
    //   final address = Utf8.fromUtf8(e.addressOf.cast());
    //   print(address);
    // });
    return Provider.of<EccWalletManager>(ctx, listen: false)
        .getFeeForTx(amount);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: MediaQuery.of(context).viewInsets,
      decoration: BoxDecoration(
        color: Color(0xff292c2d),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              IconButton(
                icon: Icon(
                  Icons.close,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Send',
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: TextField(
                    focusNode: _toFocusnode,
                    controller: _toController,
                    style: Theme.of(context).textTheme.bodyText1,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'To',
                    ),
                  ),
                ),
              ),
              IconButton(icon: Icon(Icons.paste), onPressed: () {}),
              IconButton(
                icon: Icon(Icons.qr_code),
                onPressed: () async {
                  final result = await showModalBottomSheet(
                    context: context,
                    builder: (_) => QRScanner(),
                  );
                  try {
                    final uriString = Bip21.decode(result, 'ecc');
                    print(uriString);
                    // set varibles in controllers.
                    _toController.text = uriString.address;
                    _amountController.text = uriString.amount.toString();
                    _memoController.text = uriString.options.containsKey('memo')
                        ? uriString.options['memo']
                        : '';
                  } catch (e) {
                    print(e);
                  }
                },
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: TextField(
                    focusNode: _amountFocusNode,
                    controller: _amountController,
                    style: Theme.of(context).textTheme.bodyText1,
                    keyboardType: TextInputType.numberWithOptions(
                      decimal: true,
                    ),
                    decoration: InputDecoration(
                      hintText: 'Amount',
                      suffix: Text(
                        'ECC',
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: TextField(
                    focusNode: _memoFocusNode,
                    controller: _memoController,
                    style: Theme.of(context).textTheme.bodyText1,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Memo',
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(10),
                child: RaisedButton(
                  color: Theme.of(context).accentColor,
                  child: Text(
                    'Send',
                  ),
                  onPressed: () {
                    final amount = _getfeeForTx(context, 1);
                    print(amount);
                  },
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
