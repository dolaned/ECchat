import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:provider/provider.dart';
import '../../models/eccwalletmanager.dart';
import 'dart:convert';
import 'package:bip21/bip21.dart';

class WalletReceive extends StatefulWidget {
  String _receiveAddress;

  WalletReceive(this._receiveAddress);

  static const routeName = '/wallet-receive';

  @override
  _WalletReceiveState createState() => _WalletReceiveState();
}

class _WalletReceiveState extends State<WalletReceive> {
  String uri;
  String label;
  double amount;
  String message;
  bool isRequest = false;

  @override
  void initState() {
    super.initState();

    setState(() {
      uri = Bip21.encode(
        BitcoinRequest(address: widget._receiveAddress),
        'ecc',
      );
    });
  }

  Widget _simpleReceive(BuildContext ctx) {
    return Container(
      padding: MediaQuery.of(context).viewInsets,
      height: 420,
      decoration: BoxDecoration(color: Color(0xff292c2d)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              IconButton(
                icon: Icon(
                  Icons.close,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Receive',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                decoration: BoxDecoration(color: Colors.white),
                child: QrImage(
                  data: this.uri,
                  version: QrVersions.auto,
                  size: 200.0,
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(widget._receiveAddress),
              ],
            ),
          ),
          Container(
            width: 150,
            child: RaisedButton(
              onPressed: () {},
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Share'),
                  Icon(Icons.share),
                ],
              ),
            ),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(10),
                child: RaisedButton(
                  color: Theme.of(context).accentColor,
                  child: Text(
                    'Create Request',
                  ),
                  onPressed: () {
                    setState(() {
                      isRequest = true;
                    });
                  },
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _requestReceive(BuildContext ctx) {
    return Container(
      padding: MediaQuery.of(context).viewInsets,
      height: 700,
      decoration: BoxDecoration(color: Color(0xff292c2d)),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                IconButton(
                  icon: Icon(
                    Icons.close,
                  ),
                  onPressed: () {},
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Receive',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(color: Colors.white),
                  child: QrImage(
                    data: this.uri,
                    version: QrVersions.auto,
                    size: 200.0,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget._receiveAddress,
                  )
                ],
              ),
            ),
            Container(
              width: 150,
              child: RaisedButton(
                onPressed: () {},
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Share'),
                    Icon(Icons.share),
                  ],
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      onChanged: (String input) {
                        print(input);
                        setState(() {
                          this.amount = num.parse(input);
                          this.uri = uri = Bip21.encode(
                            BitcoinRequest(
                              address: widget._receiveAddress,
                              options: {
                                'amount': this.amount,
                                'label': this.label,
                                'message': this.message
                              },
                            ),
                            'ecc',
                          );
                        });
                      },
                      style: Theme.of(context).textTheme.bodyText1,
                      keyboardType: TextInputType.numberWithOptions(
                        decimal: true,
                      ),
                      decoration: InputDecoration(
                        hintText: 'Amount',
                        suffix: Text(
                          'ECC',
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      style: Theme.of(ctx).textTheme.bodyText1,
                      decoration: InputDecoration(
                        hintText: 'Label',
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      style: Theme.of(ctx).textTheme.bodyText1,
                      decoration: InputDecoration(
                        hintText: 'Message',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!isRequest) {
      return _simpleReceive(context);
    } else {
      return _requestReceive(context);
    }
  }
}
