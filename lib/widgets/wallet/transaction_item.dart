import 'package:flutter/material.dart';
import '../../models/tranactions.dart';

class TransactionItem extends StatelessWidget {
  String txId;
  double amount;
  String to;
  String from;
  Direction direction;
  DateTime createdAt;
  TransactionItem({
    this.txId,
    this.amount,
    this.to,
    this.from,
    this.createdAt,
    this.direction,
  });
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: direction == Direction.Send
          ? Icon(
              Icons.arrow_upward,
              color: Theme.of(context).iconTheme.color,
            )
          : Icon(
              Icons.arrow_downward,
              color: Theme.of(context).iconTheme.color,
            ),
      title: Column(
        children: [
          Text(
            '$amount ECC',
            style: Theme.of(context).textTheme.headline6,
          ),
        ],
      ),
      trailing: Text('23:59'),
    );
  }
}
