import 'package:flutter/material.dart';

class ContactItem extends StatelessWidget {
  final String id;
  final String username;
  final String imageUrl;

  ContactItem(this.id, this.username, this.imageUrl);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Container(
        // padding: const EdgeInsets.all(5),
        child: CircleAvatar(
          radius: 15,
          backgroundImage: NetworkImage(imageUrl),
        ),
      ),
      title: Text(username, style: Theme.of(context).textTheme.bodyText1),
      subtitle: username.length < 10
          ? Text(
              '@$username',
              style: Theme.of(context).textTheme.bodyText2,
            )
          : null,
    );
  }
}
