import 'package:flutter/material.dart';
import '../screens/conversations/message_screen.dart';

class ConversationItem extends StatelessWidget {
  final String id;
  final String username;
  final String imageUrl;

  ConversationItem(this.id, this.username, this.imageUrl);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 15,
      ),
      onTap: () {
        Navigator.of(context).pushNamed(MessageScreen.routeName);
      },
      leading: Container(
        child: CircleAvatar(
          radius: 30,
          backgroundImage: NetworkImage(imageUrl),
        ),
      ),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(username, style: Theme.of(context).textTheme.bodyText1),
          Text('Hello how are you....',
              style: Theme.of(context).textTheme.bodyText1),
        ],
      ),
      trailing: Text('23:59'),
    );
  }
}
