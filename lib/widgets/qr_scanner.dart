import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

const flashOn = 'FLASH ON';
const flashOff = 'FLASH OFF';
const frontCamera = 'FRONT CAMERA';
const backCamera = 'BACK CAMERA';

class QRScanner extends StatefulWidget {
  static const routeName = '/qr-scanner';
  const QRScanner({
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _QRScanner();
}

class _QRScanner extends State<QRScanner> {
  String qrText;
  var flashState = flashOn;
  var cameraState = frontCamera;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
        actions: [
          IconButton(
            icon: _isFlashOn(flashState)
                ? Icon(Icons.flash_on)
                : Icon(Icons.flash_off),
            onPressed: () {
              if (controller != null) {
                controller.toggleFlash();
                if (_isFlashOn(flashState)) {
                  setState(() {
                    flashState = flashOff;
                  });
                } else {
                  setState(() {
                    flashState = flashOn;
                  });
                }
              }
            },
          ),
          IconButton(
            icon: Icon(Icons.flip_camera_ios),
            onPressed: () {
              if (controller != null) {
                controller.flipCamera();
                if (_isBackCamera(cameraState)) {
                  setState(() {
                    cameraState = frontCamera;
                  });
                } else {
                  setState(() {
                    cameraState = backCamera;
                  });
                }
              }
            },
          ),
          if (qrText != null)
            IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                print(qrText);
                Navigator.of(context).pop(qrText);
              },
            )
        ],
      ),
      backgroundColor: Colors.transparent,
      body: QRView(
        key: qrKey,
        onQRViewCreated: (QRViewController controller) {
          _onQRViewCreated(controller, context);
        },
        overlay: QrScannerOverlayShape(
          borderColor:
              qrText == null ? Theme.of(context).accentColor : Colors.green,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: 300,
        ),
      ),
    );
  }

  bool _isFlashOn(String current) {
    return flashOn == current;
  }

  bool _isBackCamera(String current) {
    return backCamera == current;
  }

  void _onQRViewCreated(QRViewController controller, BuildContext ctx) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        qrText = scanData;
      });
      controller.pauseCamera();
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
