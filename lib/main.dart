import 'package:Ecchat/models/eccwalletmanager.dart';
import 'package:Ecchat/screens/lockscreens/lockscreen.dart';
import 'package:Ecchat/screens/lockscreens/setup_lockscreen.dart';
import 'package:Ecchat/screens/settings.dart';
import 'package:Ecchat/screens/splash_screen.dart';
import 'package:Ecchat/screens/start_screen.dart';
import 'package:Ecchat/screens/transactions/transactions_screen.dart';
import 'package:Ecchat/widgets/qr_scanner.dart';
import 'package:Ecchat/widgets/wallet/wallet_send.dart';
import 'package:provider/provider.dart';
import './screens/wallet/import_wallet.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'dart:ffi';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './screens/conversations/message_screen.dart';
import 'package:path_provider/path_provider.dart' as path;

import './screens/home_tabs_screen.dart';
import 'package:flutter/material.dart';
import './models/dart_c_objects/dart_c_merkleblock.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDirectory = await path.getApplicationDocumentsDirectory();
  await Hive.initFlutter(appDirectory.path);
  brCore.InitDartApiDL(NativeApi.initializeApiDLData);
  Hive.registerAdapter(DartCMerkleBlockAdapter());

  //used to clear shared preferences on first start.
  final prefs = await SharedPreferences.getInstance();

  if (prefs.getBool('first_run') ?? true) {
    FlutterSecureStorage storage = FlutterSecureStorage();

    await storage.deleteAll();

    prefs.setBool('first_run', false);
  }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Future<int> checkAppState() async {}
  Future<bool> checkForLogin() async {
    final walletSettings = await Hive.openBox('wallet_settings');
    return walletSettings.isEmpty;
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => EccWalletManager(),
      child: Consumer<EccWalletManager>(
        builder: (ctx, wallet, _) => MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Ecchat',
          theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primaryColor: const Color(0xff1c1c23),
            accentColor: Colors.orangeAccent,
            hintColor: Color(0xffcecece),
            iconTheme: IconThemeData(
              color: Color(0xffcecece),
            ),
            // This makes the visual density adapt to the platform that you run
            // the app on. For desktop platforms, the controls will be smaller and
            // closer together (more dense) than on mobile platforms.
            visualDensity: VisualDensity.adaptivePlatformDensity,
            textTheme: TextTheme(
              bodyText1: TextStyle(
                fontSize: 14,
                fontFamily: 'Raleway',
                color: Color(0xffcecece),
              ),
              bodyText2: TextStyle(
                fontSize: 14,
                fontFamily: 'Raleway',
                color: Color(0xffcecece),
              ),
              headline1: TextStyle(
                fontSize: 20,
                fontFamily: 'Raleway',
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
              headline6: TextStyle(
                fontSize: 20,
                fontFamily: 'Raleway',
                fontWeight: FontWeight.bold,
                color: Color(0xffcecece),
              ),
            ),
          ),
          // home: LockScreen(),
          home: FutureBuilder(
            future: this.checkForLogin(),
            builder: (ctx, resultSnapshot) {
              if (resultSnapshot.connectionState == ConnectionState.waiting) {
                return SplashScreen();
              }
              return resultSnapshot.data == true ? StartScreen() : LockScreen();
            },
          ),
          routes: {
            LockScreen.routeName: (_) => LockScreen(),
            SetupLockscreen.routeName: (_) => SetupLockscreen(),
            HomeTabsScreen.routeName: (_) => HomeTabsScreen(),
            MessageScreen.routeName: (_) => MessageScreen(),
            Settings.routeName: (_) => Settings(),
            QRScanner.routeName: (_) => QRScanner(),
            ImportWallet.routeName: (_) => ImportWallet(),
            WalletSend.routeName: (context) => ChangeNotifierProvider.value(
                  value: wallet,
                  child: WalletSend(),
                ),
            TransactionsScreen.routeName: (_) => TransactionsScreen()
          },
        ),
      ),
    );
  }
}
