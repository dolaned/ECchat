import 'dart:typed_data';

class Helpers {
  static int parseInt32(Uint8List value) {
    var blob = ByteData.sublistView(value);
    return blob.getInt32(0, Endian.little);
  }

  static Uint8List convertToInt32(int value) {
    return Uint8List(4)..buffer.asByteData().setInt32(0, value, Endian.little);
  }

  static int parseUint32(Uint8List value) {
    var blob = ByteData.sublistView(value);
    return blob.getUint32(0, Endian.little);
  }

  static Uint8List convertToUint32(int value) {
    return Uint8List(4)..buffer.asByteData().setUint32(0, value, Endian.little);
  }

  static int parseUint64(Uint8List value) {
    var blob = ByteData.sublistView(value);
    return blob.getUint64(0, Endian.little);
  }

  static Uint8List convertToUint64(int value) {
    return Uint8List(8)..buffer.asByteData().setUint64(0, value, Endian.little);
  }

  static List<String> parseStringList(List<dynamic> list) {
    List<String> stringArray = [];
    for (final obj in list) {
      stringArray.add(obj);
    }
    return stringArray;
  }

  static Uint8List parseUint8List(List<dynamic> list) {
    Uint8List copiedList = new Uint8List(list.length);
    for (var i = 0; i < list.length; i++) {
      copiedList[i] = list[i];
    }
    return copiedList;
  }
}
