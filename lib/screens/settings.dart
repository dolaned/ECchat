import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:hive/hive.dart';
import '../models/eccpeermanager.dart';
import 'package:provider/provider.dart';

class Settings extends StatefulWidget {
  static const routeName = '/settings';
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  Box<dynamic> walletSettings;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadSettings();
  }

  loadSettings() async {
    final ws = await Hive.openBox('wallet_settings');
    setState(() {
      walletSettings = ws;
    });
  }

  @override
  Widget build(BuildContext context) {
    final peerManager = Provider.of<ECCPeerManager>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: SettingsList(
        // backgroundColor: Theme.of(context).primaryColor,
        sections: [
          SettingsSection(
            title: 'About',
            tiles: [
              SettingsTile(
                title: 'Version',
                subtitle: 'English',
                leading: Icon(Icons.language),
                onTap: () {},
              ),
              SettingsTile.switchTile(
                title: 'Environment',
                leading: Icon(Icons.fingerprint),
                switchValue: true,
                onToggle: (bool value) {},
              ),
            ],
          ),
          SettingsSection(
            title: 'Wallet',
            tiles: [
              SettingsTile(
                title: 'Import Wallet',
                subtitle: 'English',
                leading: Icon(Icons.language),
                onTap: () {},
              ),
              SettingsTile.switchTile(
                title: 'Use Biometrics',
                leading: Icon(Icons.fingerprint),
                switchValue: walletSettings.keys.contains('can_use_biometrics')
                    ? walletSettings.get('can_use_biometrics')
                    : false,
                onToggle: (bool value) {
                  walletSettings.put('can_use_biometrics', value);
                  setState(() {});
                },
              ),
            ],
          ),
          SettingsSection(
            title: 'Manage',
            tiles: [
              SettingsTile(
                title: 'Import Wallet',
                subtitle: 'English',
                leading: Icon(Icons.language),
                onTap: () {},
              ),
              SettingsTile.switchTile(
                title: 'Use fingerprint',
                leading: Icon(Icons.fingerprint),
                switchValue: true,
                onToggle: (bool value) {},
              ),
            ],
          ),
          SettingsSection(
            title: 'Blockchain',
            tiles: [
              SettingsTile(
                title: 'Set fixed node',
                subtitle: 'English',
                leading: Icon(Icons.language),
                onTap: () {
                  print(peerManager.isConnected);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
