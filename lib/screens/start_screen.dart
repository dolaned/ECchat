import 'package:Ecchat/screens/lockscreens/setup_lockscreen.dart';
import 'package:Ecchat/screens/wallet/import_wallet.dart';
import 'package:Ecchat/widgets/seeds/seed_phrase.dart';
import 'package:flutter/material.dart';
import 'package:bip39/bip39.dart' as bip39;
import 'package:provider/provider.dart';
import 'package:Ecchat/models/eccwalletmanager.dart';

class StartScreen extends StatefulWidget {
  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  String phrases;

  Future<void> _createPhrases() async {
    phrases = bip39.generateMnemonic();
    print(phrases);
    showModalBottomSheet(
      context: context,
      builder: (context) => SeedPhrase(phrases, _createNewWallet),
    );
  }

  void _createNewWallet(BuildContext ctx) async {
    final ms = (new DateTime.now()).millisecondsSinceEpoch;
    final secondsSinceEpoch = (ms / 1000).round();
    Provider.of<EccWalletManager>(ctx, listen: false)
        .initWallet(phrases, secondsSinceEpoch);
    await Future.delayed(Duration(milliseconds: 700));
    Navigator.of(ctx).pushReplacementNamed(SetupLockscreen.routeName);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/images/ecc_logo.png'),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text('Welcome to ECChat'),
                ),
              ),
              ButtonTheme(
                minWidth: 200,
                child: RaisedButton(
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(9),
                  ),
                  color: Theme.of(context).accentColor,
                  onPressed: _createPhrases,
                  child: Text('Create New Wallet'),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ButtonTheme(
                minWidth: 200,
                child: RaisedButton(
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(9),
                  ),
                  color: Theme.of(context).accentColor,
                  onPressed: () {
                    Navigator.of(context).pushNamed(ImportWallet.routeName);
                  },
                  child: Text('Import Wallet'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
