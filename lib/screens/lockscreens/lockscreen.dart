import 'package:Ecchat/screens/home_tabs_screen.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';
import 'package:local_auth/local_auth.dart';
import 'dart:io';

class LockScreen extends StatefulWidget {
  static const routeName = '/lockscreen';
  @override
  _LockScreenState createState() => _LockScreenState();
}

class _LockScreenState extends State<LockScreen> {
  GlobalKey<ScaffoldState> _scaffoldStateKey = GlobalKey();

  TextEditingController _pinController = TextEditingController();
  FocusNode _pinFocusNode = FocusNode();

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      border: Border.all(color: Theme.of(context).accentColor),
      borderRadius: BorderRadius.circular(15.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldStateKey,
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text('Enter Passcode'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('assets/images/ecc_logo.png'),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
                child: Text(
              'Set your passcode',
              style: Theme.of(context).textTheme.bodyText1,
            )),
          ),
          Container(
            margin: const EdgeInsets.all(20.0),
            padding: const EdgeInsets.all(20.0),
            child: PinPut(
              fieldsCount: 4,
              onSubmit: (String pin) async {
                FocusScope.of(context).unfocus();
                final store = new FlutterSecureStorage();
                String passcode = await store.read(key: 'passcode');

                if (passcode != null && passcode == pin) {
                  await Future.delayed(Duration(milliseconds: 700));

                  Navigator.of(context)
                      .pushReplacementNamed(HomeTabsScreen.routeName);
                } else {
                  _showSnackBar(
                    'Incorrect passcode please try again',
                    Colors.red,
                  );
                  _pinController.text = '';
                }
              },
              focusNode: _pinFocusNode,
              controller: _pinController,
              submittedFieldDecoration: _pinPutDecoration.copyWith(
                borderRadius: BorderRadius.circular(20.0),
              ),
              selectedFieldDecoration: _pinPutDecoration,
              followingFieldDecoration: _pinPutDecoration.copyWith(
                borderRadius: BorderRadius.circular(5.0),
                border: Border.all(
                  color: Theme.of(context).accentColor.withOpacity(.5),
                ),
              ),
            ),
          ),
          const SizedBox(height: 30.0),
          const Divider(),
          IconButton(
            icon: Icon(Icons.fingerprint),
            iconSize: 45,
            color: Theme.of(context).iconTheme.color,
            onPressed: _checkUseBiometrics,
          )
        ],
      ),
    );
  }

  _checkUseBiometrics() async {
    final wallet = await Hive.openBox('wallet_settings');
    final bool useBiometrics = await wallet.get('can_use_biometrics');
    final LocalAuthentication localAuth = LocalAuthentication();

    bool canCheckBiometrics = await localAuth.canCheckBiometrics;
    bool didAuth = false;

    // If useBiometrics is enabled, then show fingerprint auth screen
    if (useBiometrics != null && useBiometrics && canCheckBiometrics) {
      List<BiometricType> availableSystems =
          await localAuth.getAvailableBiometrics();

      if (Platform.isIOS) {
        if (availableSystems.contains(BiometricType.face)) {
          didAuth = await localAuth.authenticateWithBiometrics(
            localizedReason: 'Please authenticate to use wallet securely',
          );
        } else if (availableSystems.contains(BiometricType.fingerprint)) {
          didAuth = await localAuth.authenticateWithBiometrics(
            localizedReason: 'Please authenticate to use wallet securely',
          );
        }
      } else if (Platform.isAndroid) {
        if (availableSystems.contains(BiometricType.fingerprint)) {
          didAuth = await localAuth.authenticateWithBiometrics(
            localizedReason: 'Please authenticate to use wallet securely',
          );
        }
      }
      if (didAuth && canCheckBiometrics) {
        Navigator.of(context).pushReplacementNamed(HomeTabsScreen.routeName);
      }
    }
    _showSnackBar(
      'Cant authenticate, please use passcode instead',
      Colors.red,
    );
  }

  void _showSnackBar(String message, Color color) {
    final snackBar = SnackBar(
      duration: const Duration(seconds: 3),
      content: Container(
        height: 20,
        child: Center(
          child: Text(
            message,
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
      ),
      backgroundColor: color,
    );
    _scaffoldStateKey.currentState
      ..hideCurrentSnackBar()
      ..showSnackBar(snackBar);
  }
}
