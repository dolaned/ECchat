import 'package:Ecchat/screens/home_tabs_screen.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';

class SetupLockscreen extends StatefulWidget {
  static const routeName = '/setup-lockscreen';
  @override
  _SetupLockscreenState createState() => _SetupLockscreenState();
}

class _SetupLockscreenState extends State<SetupLockscreen> {
  PageController _pageController =
      PageController(initialPage: 0, keepPage: true);
  GlobalKey<ScaffoldState> _scaffoldStateKey = GlobalKey<ScaffoldState>();

  TextEditingController _pinEnterController = TextEditingController();
  TextEditingController _pinConfirmationController = TextEditingController();

  FocusNode _pinEnterFocusNode = FocusNode();
  FocusNode _pinConfirmFocusNode = FocusNode();

  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      border: Border.all(color: Theme.of(context).accentColor),
      borderRadius: BorderRadius.circular(15.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      key: _scaffoldStateKey,
      appBar: AppBar(
        title: Text('Create Passcode'),
      ),
      body: SafeArea(
        child: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: _pageController,
          children: [
            GestureDetector(
              onTap: () {
                _pinEnterFocusNode.unfocus();
              },
              child: Scaffold(
                backgroundColor: Theme.of(context).primaryColor,
                body: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('assets/images/ecc_logo.png'),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                          child: Text(
                        'Set your passcode',
                        style: Theme.of(context).textTheme.bodyText1,
                      )),
                    ),
                    Container(
                      margin: const EdgeInsets.all(20.0),
                      padding: const EdgeInsets.all(20.0),
                      child: PinPut(
                        fieldsCount: 4,
                        onSubmit: (String pin) {
                          _pageController.nextPage(
                            duration: Duration(milliseconds: 300),
                            curve: Curves.easeIn,
                          );
                          FocusScope.of(context).unfocus();
                        },
                        focusNode: _pinEnterFocusNode,
                        controller: _pinEnterController,
                        submittedFieldDecoration: _pinPutDecoration.copyWith(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        selectedFieldDecoration: _pinPutDecoration,
                        followingFieldDecoration: _pinPutDecoration.copyWith(
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all(
                            color:
                                Theme.of(context).accentColor.withOpacity(.5),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 30.0),
                    const Divider(),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                _pinConfirmFocusNode.unfocus();
              },
              child: Scaffold(
                backgroundColor: Theme.of(context).primaryColor,
                body: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset('assets/images/ecc_logo.png'),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                          child: Text(
                        'Confirm your passcode',
                        style: Theme.of(context).textTheme.bodyText1,
                      )),
                    ),
                    Container(
                      margin: const EdgeInsets.all(20.0),
                      padding: const EdgeInsets.all(20.0),
                      child: PinPut(
                        fieldsCount: 4,
                        onSubmit: (String pin) => _validate(pin, context),
                        focusNode: _pinConfirmFocusNode,
                        controller: _pinConfirmationController,
                        submittedFieldDecoration: _pinPutDecoration.copyWith(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        selectedFieldDecoration: _pinPutDecoration,
                        followingFieldDecoration: _pinPutDecoration.copyWith(
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all(
                            color:
                                Theme.of(context).accentColor.withOpacity(.5),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 30.0),
                    const Divider(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showSnackBar(String message, Color color) {
    final snackBar = SnackBar(
      duration: const Duration(seconds: 3),
      content: Container(
        height: 20,
        child: Center(
          child: Text(
            message,
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
      ),
      backgroundColor: color,
    );
    _scaffoldStateKey.currentState
      ..hideCurrentSnackBar()
      ..showSnackBar(snackBar);
  }

  Future<void> _validate(String pin, BuildContext ctx) async {
    // pins match save them to secure storage
    if (_pinEnterController.text == _pinConfirmationController.text) {
      FocusScope.of(ctx).unfocus();

      final store = new FlutterSecureStorage();
      await store.write(key: 'passcode', value: pin);
      final misc = await Hive.openBox('wallet_settings');
      await misc.put('passcode', true);
      await misc.put('can_use_biometrics', true);

      await Future.delayed(Duration(milliseconds: 700));

      Navigator.of(ctx).pushReplacementNamed(HomeTabsScreen.routeName);
    }
    //pins dont match restart
    else {
      FocusScope.of(context).unfocus();
      _showSnackBar(
        'Passcodes did not match please try again!',
        Theme.of(ctx).errorColor,
      );
      _pageController.animateTo(
        0,
        duration: Duration(milliseconds: 500),
        curve: Curves.easeInBack,
      );
      _pinEnterController.text = '';
      _pinConfirmationController.text = '';
    }
  }
}
