import '../../widgets/conversations_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/conversations.dart';

class ConversationListScreen extends StatefulWidget {
  @override
  _ConversationListScreenState createState() => _ConversationListScreenState();
}

class _ConversationListScreenState extends State<ConversationListScreen> {
  Widget getAppBar() {
    return AppBar(
      leading: Container(
        padding: const EdgeInsets.all(15),
        child: CircleAvatar(
          backgroundImage: AssetImage('assets/images/default_user.jpg'),
          radius: 35,
        ),
      ),
      title: Text('Dolaned'),
    );
  }

  @override
  Widget build(BuildContext context) {
    var conversations =
        Provider.of<Conversations>(context, listen: false).items;
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: getAppBar(),
      body: Container(
        child: ListView.builder(
          itemCount: conversations.length,
          itemBuilder: (ctx, i) => Column(
            children: [
              ConversationItem(
                conversations[i].id,
                conversations[i].name,
                conversations[i].imageUrl,
              ),
              Divider(
                indent: 50,
                color: Color.fromARGB(50, 206, 206, 206),
                endIndent: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
