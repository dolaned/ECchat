import '../../widgets/contact_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/contacts.dart';

class ContactsListScreen extends StatelessWidget {
  Widget getAppBar(BuildContext context) {
    return AppBar(
      title: Padding(
        padding: const EdgeInsets.only(left: 100),
        child: TextField(
          style: Theme.of(context).textTheme.bodyText1,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: 'Enter a search term',
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    var contacts = Provider.of<Contacts>(context, listen: false).items;
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: getAppBar(context),
      body: Container(
        child: ListView.builder(
          itemCount: contacts.length,
          itemBuilder: (ctx, i) => Column(
            children: [
              ContactItem(
                contacts[i].id,
                contacts[i].username,
                contacts[i].imageUrl,
              ),
              Divider(
                indent: 50,
                color: Color.fromARGB(50, 206, 206, 206),
                endIndent: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
