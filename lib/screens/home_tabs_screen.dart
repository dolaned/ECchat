import 'package:Ecchat/models/eccpeermanager.dart';
import 'package:Ecchat/models/tranactions.dart';
import 'package:Ecchat/models/eccwalletmanager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../models/contacts.dart';
import '../models/conversations.dart';
import '../screens/contacts/contacts_list_screen.dart';
import '../screens/conversations/conversation_list_screen.dart';
import '../screens/wallet/wallet_home.dart';

class HomeTabsScreen extends StatefulWidget {
  static const routeName = '/home-screen';
  @override
  _HomeTabsScreenState createState() => _HomeTabsScreenState();
}

class _HomeTabsScreenState extends State<HomeTabsScreen> {
  List<Map<String, Object>> _pages;

  @override
  initState() {
    _pages = [
      {'page': ContactsListScreen(), 'title': 'Contacts'},
      {'page': ConversationListScreen(), 'title': 'Conversations'},
      {'page': WalletHome(), 'title': 'Wallet'},
    ];
    Provider.of<EccWalletManager>(context, listen: false)
        .initWallet(null, null);
    super.initState();
  }

  int _selectedPageIndex = 0;
  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<EccWalletManager>(
      builder: (ctx, wallet, _) => MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => ECCPeerManager(wallet.wallet),
          ),
          ChangeNotifierProvider(
            create: (_) => Contacts(),
          ),
          ChangeNotifierProvider(
            create: (_) => Conversations(),
          ),
          ChangeNotifierProvider(
            create: (_) => Transactions(),
          ),
        ],
        child: Scaffold(
          body: _pages[_selectedPageIndex]['page'],
          bottomNavigationBar: BottomNavigationBar(
            onTap: _selectPage,
            backgroundColor: Color(0xff292c2d),
            unselectedItemColor: Colors.white54,
            selectedItemColor: Theme.of(context).accentColor,
            currentIndex: _selectedPageIndex,
            items: [
              BottomNavigationBarItem(
                backgroundColor: Theme.of(context).primaryColor,
                icon: Icon(Icons.contacts),
                title: Text('Contacts'),
              ),
              BottomNavigationBarItem(
                backgroundColor: Theme.of(context).primaryColor,
                icon: Icon(Icons.chat),
                title: Text('Conversations'),
              ),
              BottomNavigationBarItem(
                backgroundColor: Theme.of(context).primaryColor,
                icon: Icon(Icons.attach_money),
                title: Text('Wallet'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
