import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/eccwalletmanager.dart';
import '../../screens/lockscreens/setup_lockscreen.dart';
import 'package:bip39/bip39.dart' as bip39;

class ImportWallet extends StatefulWidget {
  static const routeName = '/import-wallet';
  @override
  _ImportWalletState createState() => _ImportWalletState();

  void _createNewWallet(BuildContext ctx, String phrases) async {
    Provider.of<EccWalletManager>(ctx, listen: false)
        .initWallet(phrases, 1464739200);
    await Future.delayed(Duration(milliseconds: 700));
    Navigator.of(ctx).pushReplacementNamed(SetupLockscreen.routeName);
  }
}

class _ImportWalletState extends State<ImportWallet> {
  var phrases = [
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  ];

  final _formKey = GlobalKey<FormState>();
  // setup focus nodes and controllers for all 12 inputs

  FocusNode _inputOneFocusNode = FocusNode();
  TextEditingController _inputOneController = TextEditingController();

  FocusNode _inputTwoFocusNode = FocusNode();
  TextEditingController _inputTwoController = TextEditingController();

  FocusNode _inputThreeFocusNode = FocusNode();
  TextEditingController _inputThreeController = TextEditingController();

  FocusNode _inputFourFocusNode = FocusNode();
  TextEditingController _inputFourController = TextEditingController();

  FocusNode _inputFiveFocusNode = FocusNode();
  TextEditingController _inputFiveController = TextEditingController();

  FocusNode _inputSixFocusNode = FocusNode();
  TextEditingController _inputSixController = TextEditingController();

  FocusNode _inputSevenFocusNode = FocusNode();
  TextEditingController _inputSevenController = TextEditingController();

  FocusNode _inputEightFocusNode = FocusNode();
  TextEditingController _inputEightController = TextEditingController();

  FocusNode _inputNineFocusNode = FocusNode();
  TextEditingController _inputNineController = TextEditingController();

  FocusNode _inputTenFocusNode = FocusNode();
  TextEditingController _inputTenController = TextEditingController();

  FocusNode _inputElevenFocusNode = FocusNode();
  TextEditingController _inputElevenController = TextEditingController();

  FocusNode _inputTwelveFocusNode = FocusNode();
  TextEditingController _inputTwelveController = TextEditingController();

  void validatePhrases(BuildContext ctx) {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      // If the form is valid, display a snackbar. In the real world,
      // you'd often call a server or save the information in a database.
      print(phrases.toString());

      final phraseString = phrases.join(' ');
      print(phraseString);
      final validated = bip39.validateMnemonic(phraseString);
      if (validated) {
        widget._createNewWallet(ctx, phraseString);
      }
      print(validated);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Import Wallet'),
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputOneFocusNode,
                          controller: _inputOneController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phase One',
                          ),
                          onSaved: (value) {
                            phrases[0] = value;
                          },
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputTwoFocusNode,
                          controller: _inputTwoController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phase Two',
                          ),
                          onSaved: (value) {
                            phrases[1] = value;
                          },
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputThreeFocusNode,
                          controller: _inputThreeController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phrase Three',
                          ),
                          onSaved: (value) {
                            phrases[2] = value;
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputFourFocusNode,
                          controller: _inputFourController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phase Four',
                          ),
                          onSaved: (value) {
                            phrases[3] = value;
                          },
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputFiveFocusNode,
                          controller: _inputFiveController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phase Five',
                          ),
                          onSaved: (value) {
                            phrases[4] = value;
                          },
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputSixFocusNode,
                          controller: _inputSixController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phrase Six',
                          ),
                          onSaved: (value) {
                            phrases[5] = value;
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputSevenFocusNode,
                          controller: _inputSevenController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phase Seven',
                          ),
                          onSaved: (value) {
                            phrases[6] = value;
                          },
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputEightFocusNode,
                          controller: _inputEightController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phase Eight',
                          ),
                          onSaved: (value) {
                            phrases[7] = value;
                          },
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputNineFocusNode,
                          controller: _inputNineController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phrase Nine',
                          ),
                          onSaved: (value) {
                            phrases[8] = value;
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputTenFocusNode,
                          controller: _inputTenController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phase Ten',
                          ),
                          onSaved: (value) {
                            phrases[9] = value;
                          },
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputElevenFocusNode,
                          controller: _inputElevenController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phase Eleven',
                          ),
                          onSaved: (value) {
                            phrases[10] = value;
                          },
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          focusNode: _inputTwelveFocusNode,
                          controller: _inputTwelveController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please a phrase';
                            }
                            return null;
                          },
                          style: Theme.of(context).textTheme.bodyText1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'Phrase Twelve',
                          ),
                          onSaved: (value) {
                            phrases[11] = value;
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width - 40,
                      padding: EdgeInsets.all(10),
                      child: RaisedButton(
                        color: Theme.of(context).accentColor,
                        child: Text(
                          'Import Wallet',
                        ),
                        onPressed: () {
                          validatePhrases(context);
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
