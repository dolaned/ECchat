import 'package:Ecchat/models/eccpeermanager.dart';
import 'package:Ecchat/screens/transactions/transaction_list.dart';
import 'package:Ecchat/screens/transactions/transactions_screen.dart';
import 'dart:async';

import '../../widgets/wallet/wallet_receive.dart';
import '../../screens/settings.dart';
import '../../widgets/wallet/wallet_send.dart';
import '../../models/tranactions.dart';
import '../../models/eccwalletmanager.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import '../../models/dart_c_objects/dart_c_merkleblock.dart';
import 'package:hive/hive.dart';

enum MenuOptions { History, Settings, Connect, ClearBlocks }

class WalletHome extends StatefulWidget {
  @override
  _WalletHomeState createState() => _WalletHomeState();
}

class _WalletHomeState extends State<WalletHome> {
  GlobalKey<ScaffoldState> _key = GlobalKey();
  PersistentBottomSheetController _controller;
  String _now;
  Timer _everySecond;
  @override
  initState() {
    super.initState();

    // sets first value
    _now = DateTime.now().second.toString();

    // defines a timer
    _everySecond = Timer.periodic(Duration(seconds: 5), (Timer t) {
      setState(() {
        _now = DateTime.now().second.toString();
      });
    });
  }

  @override
  dispose() {
    _everySecond = null;
    super.dispose();
  }

  Widget getAppBar(BuildContext context) {
    final peerManager = Provider.of<ECCPeerManager>(context);
    final transactions = Provider.of<Transactions>(context);
    return AppBar(
      title: Text(
        'ECChat',
        style: Theme.of(context).textTheme.headline1,
      ),
      actions: [
        PopupMenuButton(
          onSelected: (MenuOptions selectedValue) async {
            if (selectedValue == MenuOptions.Connect) {
              if (peerManager.isConnected) {
                peerManager.disconnect();
              } else {
                peerManager.connect();
              }
            } else if (selectedValue == MenuOptions.Settings) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      ChangeNotifierProvider.value(
                    value: peerManager,
                    builder: (context, child) => Settings(),
                  ),
                ),
              );

              // Navigator.of(context).pushNamed(Settings.routeName);
            } else if (selectedValue == MenuOptions.History) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) =>
                      ChangeNotifierProvider.value(
                    value: transactions,
                    builder: (context, child) => TransactionsScreen(),
                  ),
                ),
              );
            } else if (selectedValue == MenuOptions.ClearBlocks) {
              final headers =
                  await Hive.openBox<DartCMerkleBlock>('merkleHeadersBox');
              headers.clear();
            }
            setState(() {});
          },
          icon: Icon(Icons.more_vert),
          itemBuilder: (context) => [
            PopupMenuItem(
              child: Text('Transaction history'),
              value: MenuOptions.History,
            ),
            PopupMenuItem(
              child: Text('Settings'),
              value: MenuOptions.Settings,
            ),
            PopupMenuItem(
              child: peerManager.isConnected
                  ? Text('Disconnect')
                  : Text('Connect'),
              value: MenuOptions.Connect,
            ),
            PopupMenuItem(
              child: Text('Clear Blocks'),
              value: MenuOptions.ClearBlocks,
            )
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var transactions = Provider.of<Transactions>(context, listen: false).items;
    var wallet = Provider.of<EccWalletManager>(context);
    var peerManager = Provider.of<ECCPeerManager>(context);
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: getAppBar(context),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(top: 5, left: 5, right: 5, bottom: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularPercentIndicator(
                    radius: 200,
                    animation: false,
                    animationDuration: 1200,
                    lineWidth: 15.0,
                    percent: peerManager.syncProgress,
                    center: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "${peerManager.syncProgress.toStringAsFixed(2)}%",
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        Text(
                          'Headers: ${peerManager.lastHeaderHeight}',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        Text(
                          'Blocks: ${peerManager.lastBlockHeight}',
                          style: Theme.of(context).textTheme.headline6,
                        )
                      ],
                    ),
                    circularStrokeCap: CircularStrokeCap.round,
                    backgroundColor: Colors.black38,
                    progressColor: Theme.of(context).accentColor,
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(wallet.balance.toString()),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'ECC',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RaisedButton(
                    padding: EdgeInsets.all(15),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(9),
                    ),
                    color: Theme.of(context).accentColor,
                    child: Text(
                      'Receive',
                    ),
                    onPressed: () {
                      showModalBottomSheet(
                          isScrollControlled: true,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.vertical(
                                  top: Radius.circular(25.0))),
                          context: context,
                          builder: (context) =>
                              WalletReceive(wallet.receiveAddress));
                    },
                  ),
                  RaisedButton(
                    padding: EdgeInsets.all(15),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(9),
                    ),
                    color: Theme.of(context).accentColor,
                    child: Text('Send'),
                    onPressed: () {
                      showModalBottomSheet(
                        isScrollControlled: true,
                        context: context,
                        builder: (BuildContext ctx) => SingleChildScrollView(
                          child: Container(
                              padding: EdgeInsets.only(
                                  bottom:
                                      MediaQuery.of(context).viewInsets.bottom),
                              child: WalletSend()),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            Container(
              height: 280,
              child: TransactionList(transactions),
            ),
          ],
        ),
      ),
    );
  }
}
