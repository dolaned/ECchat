import 'package:Ecchat/screens/transactions/transaction_list.dart';
import 'package:flutter/material.dart';
import '../../models/tranactions.dart';
import 'package:provider/provider.dart';

class TransactionsScreen extends StatelessWidget {
  static const routeName = '/transactions';

  @override
  Widget build(BuildContext context) {
    var _transactions = Provider.of<Transactions>(context, listen: false).items;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'ECChat',
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Container(
        child: TransactionList(_transactions),
      ),
    );
  }
}
