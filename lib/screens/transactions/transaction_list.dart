import 'package:flutter/material.dart';
import '../../models/tranactions.dart';
import '../../widgets/wallet/transaction_item.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> _transactions;
  TransactionList(this._transactions);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: ListView.builder(
        itemCount: _transactions.length,
        itemBuilder: (ctx, i) => Column(
          children: [
            TransactionItem(
              txId: _transactions[i].txId,
              to: _transactions[i].to,
              from: _transactions[i].from,
              createdAt: _transactions[i].createdAt,
              amount: _transactions[i].amount,
              direction: _transactions[i].direction,
            ),
            Divider(
              indent: 50,
              color: Color.fromARGB(50, 206, 206, 206),
              endIndent: 10,
            )
          ],
        ),
      ),
    );
  }
}
