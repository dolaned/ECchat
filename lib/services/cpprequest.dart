import 'package:flutter/foundation.dart';

class CppRequest {
  final String method;
  final List<dynamic> data;

  factory CppRequest.fromCppMessage(List message) {
    return CppRequest._(message[0], message[1]);
  }

  CppRequest._(this.method, this.data);

  String toString() => 'CppRequest(method: $method, ${data.length} bytes)';
}
