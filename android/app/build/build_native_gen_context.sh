#!/bin/sh

cd "/Users/dylanaird/projects/ECchat/android/app/native"
"/usr/local/Cellar/cmake/3.18.3/bin/cmake" --build "/Users/dylanaird/projects/ECchat/android/app/native" --target "gen_context"
"/usr/local/Cellar/cmake/3.18.3/bin/cmake" -E create_symlink "/Users/dylanaird/projects/ECchat/android/app/native/build/gen_context" "/Users/dylanaird/projects/ECchat/android/app/build/native-gen_context"

# Ok let's generate a depfile if we can.
if test "xUnix Makefiles" = "xNinja"; then
    "/Users/dylanaird/projects/ECchat/android/app/cmake/utils/gen-ninja-deps.py" \
        --build-dir "/Users/dylanaird/projects/ECchat/android/app/native" \
        --base-dir "/Users/dylanaird/projects/ECchat/android/app" \
        --ninja "/usr/bin/make" \
        "build/native-gen_context" "gen_context" > "/Users/dylanaird/projects/ECchat/android/app/build/native-gen_context.d"
fi
