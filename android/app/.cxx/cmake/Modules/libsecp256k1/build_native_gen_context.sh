#!/bin/sh

cd "/Users/dylanaird/projects/ECchat/android/app/.cxx/cmake/debug/armeabi-v7a/native"
"/Users/dylanaird/Library/Android/sdk/cmake/3.10.2.4988404/bin/cmake" --build "/Users/dylanaird/projects/ECchat/android/app/.cxx/cmake/debug/armeabi-v7a/native" --target "../../Modules/libsecp256k1/gen_context"
"/Users/dylanaird/Library/Android/sdk/cmake/3.10.2.4988404/bin/cmake" -E create_symlink "/Users/dylanaird/projects/ECchat/android/app/.cxx/cmake/debug/armeabi-v7a/native/../../Modules/libsecp256k1/gen_context" "/Users/dylanaird/projects/ECchat/android/app/.cxx/cmake/Modules/libsecp256k1/native-gen_context"

# Ok let's generate a depfile if we can.
if test "xNinja" = "xNinja"; then
    "/Users/dylanaird/projects/ECchat/android/app/cmake/utils/gen-ninja-deps.py" \
        --build-dir "/Users/dylanaird/projects/ECchat/android/app/.cxx/cmake/debug/armeabi-v7a/native" \
        --base-dir "/Users/dylanaird/projects/ECchat/android/app/.cxx/cmake/debug/armeabi-v7a" \
        --ninja "/Users/dylanaird/Library/Android/sdk/cmake/3.10.2.4988404/bin/ninja" \
        "../../Modules/libsecp256k1/native-gen_context" "../../Modules/libsecp256k1/gen_context" > "/Users/dylanaird/projects/ECchat/android/app/.cxx/cmake/Modules/libsecp256k1/native-gen_context.d"
fi
