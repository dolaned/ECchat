
#include "api.h"
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#define UInt256ToString(s) UInt256GetHex(s)
// For the typedefs, but don't use the function pointers.

void (*saveHeadersDartCallback)(int replace, u_int8_t *merkleBuffer, uint64_t num_bytes);
void (*saveBlocksDartCallback)(int replace, u_int8_t *merkleBuffer, uint64_t num_bytes);
void (*savePeersDartCallback)(int replace, u_int8_t *peerBuffer, uint64_t num_bytes);

Dart_Port peer_send_port;
Dart_Port roam_send_port;
Dart_Port wallet_send_port;

Dart_Port peer_receive_port;
Dart_Port roam_receive_port;
Dart_Port wallet_receive_port;

PeerManager *peerManager;

static void FreeFinalizer(void *Dart_WeakPersistentHandle, void *value)
{
    free(value);
}

LIBAPI intptr_t InitDartApiDL(void *data)
{
    return Dart_InitializeApiDL(data);
}

// Notify Dart through a port that the C lib has pending async callbacks.

// Expects heap allocated `work` so delete can be called on it.

// The `send_port` should be from the isolate which registered the callback.
void NotifyDart(Dart_Port send_port, Dart_CObject *dart_object)
{
    const bool result = Dart_PostCObject_DL(send_port, dart_object);
    if (!result)
    {
        // FATAL("C   :  Posting message to port failed.");
    }
}

void handlePeerResponse(Dart_Port p, Dart_CObject *message)
{
    const char *methodname = "messageReceivedDart";
    Dart_CObject c_method_name;
    c_method_name.type = Dart_CObject_kString;
    c_method_name.value.as_string = (char *)(methodname);

    Dart_CObject messageReceived;
    messageReceived.type = Dart_CObject_kBool;
    messageReceived.value.as_bool = true;

    Dart_CObject *requestData[] = {
        &messageReceived,
    };

    Dart_CObject data;
    data.type = Dart_CObject_kArray;
    data.value.as_array.values = requestData;
    data.value.as_array.length = 1;

    Dart_CObject *c_request_arr[] = {
        &c_method_name,
        &data,
    };

    Dart_CObject c_request;
    c_request.type = Dart_CObject_kArray;
    c_request.value.as_array.values = c_request_arr;
    c_request.value.as_array.length = 2;

    NotifyDart(peer_send_port, &c_request);
}
void handleRoamResponse(Dart_Port p, Dart_CObject *message)
{
}
void handleWalletResponse(Dart_Port p, Dart_CObject *message)
{
}

// this stores a reference to the saveHeaders function on the dart side
LIBAPI Dart_Port registerPeerDartPort(Dart_Port send_port)
{
    peer_send_port = send_port;
    peer_receive_port =
        Dart_NewNativePort_DL("peer-response", handlePeerResponse,
                              /*handle_concurrently=*/true);
    return peer_receive_port;
}

// this stores a reference to the saveHeaders function on the dart side
LIBAPI Dart_Port registerRoamDartPort(Dart_Port send_port)
{
    roam_send_port = send_port;
    roam_receive_port =
        Dart_NewNativePort_DL("roam-response", handleRoamResponse,
                              /*handle_concurrently=*/true);
    return roam_receive_port;
}

// this stores a reference to the saveHeaders function on the dart side
LIBAPI Dart_Port registerWalletDartPort(Dart_Port send_port)
{
    wallet_send_port = send_port;
    wallet_receive_port =
        Dart_NewNativePort_DL("wallet-response", handleWalletResponse,
                              /*handle_concurrently=*/true);
    return wallet_receive_port;
}

/**
 * Peer manager callbacks.
*/

// this is the c callback that peermangers calls with data
void saveHeaders(void *info, int replace, MerkleBlock *blocks[], uint64_t blocksCount)
{
    const char *methodname = "saveHeadersDart";
    Dart_CObject c_method_name;
    c_method_name.type = Dart_CObject_kString;
    c_method_name.value.as_string = (char *)(methodname);
    uint64_t i;

    for (i = 0; i < blocksCount; ++i)
    {
        //blockhash
        Dart_CObject blockHash;
        blockHash.type = Dart_CObject_kString;
        blockHash.value.as_string = UInt256GetHex(blocks[i]->blockHash);

        Dart_CObject version;
        version.type = Dart_CObject_kInt32;
        version.value.as_int32 = blocks[i]->version;

        Dart_CObject prevBlock;
        prevBlock.type = Dart_CObject_kString;
        prevBlock.value.as_string = UInt256GetHex(blocks[i]->prevBlock);

        Dart_CObject merkleRoot;
        merkleRoot.type = Dart_CObject_kString;
        merkleRoot.value.as_string = UInt256GetHex(blocks[i]->merkleRoot);

        uint64_t blockbuffer_bytes = 0;
        uint8_t *blockbuffer = serialize_merkleblock(blocks[i], &blockbuffer_bytes);

        Dart_CObject rawBytes;
        rawBytes.type = Dart_CObject_kTypedData;
        rawBytes.value.as_typed_data.type = Dart_TypedData_kUint8;
        rawBytes.value.as_typed_data.values = blockbuffer;
        rawBytes.value.as_typed_data.length = blockbuffer_bytes;

        Dart_CObject timeStamp;
        uint8_t *timeStampValue = malloc(sizeof(uint32_t));
        memcpy(timeStampValue, &blocks[i]->timestamp, 4);
        timeStamp.type = Dart_CObject_kTypedData;
        timeStamp.value.as_typed_data.type = Dart_TypedData_kUint8;
        timeStamp.value.as_typed_data.values = timeStampValue;
        timeStamp.value.as_typed_data.length = 4;

        Dart_CObject target;
        uint8_t *targetValue = malloc(sizeof(uint32_t));
        memcpy(targetValue, &blocks[i]->target, 4);
        target.type = Dart_CObject_kTypedData;
        target.value.as_typed_data.type = Dart_TypedData_kUint8;
        target.value.as_typed_data.values = targetValue;
        target.value.as_typed_data.length = 4;

        Dart_CObject nonce;
        uint8_t *nonceValue = malloc(sizeof(uint32_t));
        memcpy(nonceValue, &blocks[i]->nonce, 4);
        nonce.type = Dart_CObject_kTypedData;
        nonce.value.as_typed_data.type = Dart_TypedData_kUint8;
        nonce.value.as_typed_data.values = nonceValue;
        nonce.value.as_typed_data.length = 4;

        Dart_CObject totalTx;
        totalTx.type = Dart_CObject_kInt32;
        totalTx.value.as_int32 = blocks[i]->totalTx;

        Dart_CObject hashes;

        Dart_CObject hashesCount;
        hashesCount.type = Dart_CObject_kInt64;
        hashesCount.value.as_int64 = blocks[i]->hashesCount;

        Dart_CObject flags;
        //todo

        Dart_CObject flagsLen;
        flagsLen.type = Dart_CObject_kInt64;
        flagsLen.value.as_int64 = blocks[i]->flagsLen;

        Dart_CObject height;
        uint8_t *heightValue = malloc(sizeof(uint32_t));
        memcpy(heightValue, &blocks[i]->height, 4);
        height.type = Dart_CObject_kTypedData;
        height.value.as_typed_data.type = Dart_TypedData_kUint8;
        height.value.as_typed_data.values = heightValue;
        height.value.as_typed_data.length = 4;

        Dart_CObject haveData;
        haveData.type = Dart_CObject_kBool;
        haveData.value.as_bool = blocks[i]->haveData;

        Dart_CObject *txs[blocks[i]->hashesCount];

        for (size_t j = 0; j < blocks[i]->hashesCount; j++)
        {
            Dart_CObject t;
            t.type = Dart_CObject_kString;
            t.value.as_string = UInt256GetHex(blocks[i]->hashes[j]);
            txs[j] = &t;
        }

        Dart_CObject transactionHashes;
        transactionHashes.type = Dart_CObject_kArray;
        transactionHashes.value.as_array.values = txs;
        transactionHashes.value.as_array.length = blocks[i]->hashesCount;

        Dart_CObject *flagsArray[blocks[i]->flagsLen];

        for (size_t k = 0; k < blocks[i]->flagsLen; k++)
        {
            Dart_CObject f;
            f.type = Dart_CObject_kTypedData;
            f.value.as_typed_data.type = Dart_TypedData_kUint8;
            f.value.as_typed_data.values = &blocks[i]->flags[k];
            f.value.as_typed_data.length = sizeof(blocks[i]->flags[k]);
            flagsArray[k] = &f;
        }

        Dart_CObject flagObject;
        flagObject.type = Dart_CObject_kArray;
        flagObject.value.as_array.values = flagsArray;
        flagObject.value.as_array.length = blocks[i]->flagsLen;

        Dart_CObject *merkleBlockData[] = {
            &blockHash,
            &version,
            &prevBlock,
            &merkleRoot,
            &timeStamp,
            &target,
            &nonce,
            &totalTx,
            &hashesCount,
            &flagsLen,
            &height,
            &haveData,
            &transactionHashes,
            &flagObject,
            &rawBytes,
        };

        Dart_CObject merkleBlock;
        merkleBlock.type = Dart_CObject_kArray;
        merkleBlock.value.as_array.values = merkleBlockData;
        merkleBlock.value.as_array.length = 15;

        Dart_CObject dartReplace;
        dartReplace.type = Dart_CObject_kBool;
        dartReplace.value.as_bool = replace;

        Dart_CObject *requestData[] = {
            &dartReplace,
            &merkleBlock,
        };

        Dart_CObject data;
        data.type = Dart_CObject_kArray;
        data.value.as_array.values = requestData;
        data.value.as_array.length = 2;

        Dart_CObject *c_request_arr[] = {
            &c_method_name,
            &data,
        };

        Dart_CObject c_request;
        c_request.type = Dart_CObject_kArray;
        c_request.value.as_array.values = c_request_arr;
        c_request.value.as_array.length = 2;

        NotifyDart(peer_send_port, &c_request);
    }
}

void syncStarted(void *info)
{
    const char *methodname = "syncStartedDart";
    Dart_CObject c_method_name;
    c_method_name.type = Dart_CObject_kString;
    c_method_name.value.as_string = (char *)(methodname);

    Dart_CObject syncStarted;
    syncStarted.type = Dart_CObject_kBool;
    syncStarted.value.as_bool = true;

    Dart_CObject *requestData[] = {
        &syncStarted,
    };

    Dart_CObject data;
    data.type = Dart_CObject_kArray;
    data.value.as_array.values = requestData;
    data.value.as_array.length = 1;

    Dart_CObject *c_request_arr[] = {
        &c_method_name,
        &data,
    };

    Dart_CObject c_request;
    c_request.type = Dart_CObject_kArray;
    c_request.value.as_array.values = c_request_arr;
    c_request.value.as_array.length = 2;

    NotifyDart(peer_send_port, &c_request);
}

// Sync stopped callback
void syncStopped(void *info, int error)
{
    const char *methodname = "syncStoppedDart";
    Dart_CObject c_method_name;
    c_method_name.type = Dart_CObject_kString;
    c_method_name.value.as_string = (char *)(methodname);

    Dart_CObject syncError;
    syncError.type = Dart_CObject_kBool;
    syncError.value.as_bool = error;

    Dart_CObject *requestData[] = {
        &syncError,
    };

    Dart_CObject data;
    data.type = Dart_CObject_kArray;
    data.value.as_array.values = requestData;
    data.value.as_array.length = 1;

    Dart_CObject *c_request_arr[] = {
        &c_method_name,
        &data,
    };

    Dart_CObject c_request;
    c_request.type = Dart_CObject_kArray;
    c_request.value.as_array.values = c_request_arr;
    c_request.value.as_array.length = 2;

    NotifyDart(peer_send_port, &c_request);
}
void txStatusUpdate(void *info) {}
void saveBlocks(void *info, int replace, MerkleBlock *blocks[], uint64_t blocksCount)
{
    const char *methodname = "saveBlocksDart";
    Dart_CObject c_method_name;
    c_method_name.type = Dart_CObject_kString;
    c_method_name.value.as_string = (char *)(methodname);
    uint64_t i;

    for (i = 0; i < blocksCount; ++i)
    {
        //blockhash
        Dart_CObject blockHash;
        blockHash.type = Dart_CObject_kString;
        blockHash.value.as_string = UInt256GetHex(blocks[i]->blockHash);

        Dart_CObject version;
        version.type = Dart_CObject_kInt32;
        version.value.as_int32 = blocks[i]->version;

        Dart_CObject prevBlock;
        prevBlock.type = Dart_CObject_kString;
        prevBlock.value.as_string = UInt256GetHex(blocks[i]->prevBlock);

        Dart_CObject merkleRoot;
        merkleRoot.type = Dart_CObject_kString;
        merkleRoot.value.as_string = UInt256GetHex(blocks[i]->merkleRoot);

        Dart_CObject timeStamp;
        uint8_t *timeStampValue = malloc(sizeof(uint32_t));
        memcpy(timeStampValue, &blocks[i]->timestamp, 4);
        timeStamp.type = Dart_CObject_kTypedData;
        timeStamp.value.as_typed_data.type = Dart_TypedData_kUint8;
        timeStamp.value.as_typed_data.values = timeStampValue;
        timeStamp.value.as_typed_data.length = 4;

        Dart_CObject target;
        uint8_t *targetValue = malloc(sizeof(uint32_t));
        memcpy(targetValue, &blocks[i]->target, 4);
        target.type = Dart_CObject_kTypedData;
        target.value.as_typed_data.type = Dart_TypedData_kUint8;
        target.value.as_typed_data.values = targetValue;
        target.value.as_typed_data.length = 4;

        Dart_CObject nonce;
        uint8_t *nonceValue = malloc(sizeof(uint32_t));
        memcpy(nonceValue, &blocks[i]->nonce, 4);
        nonce.type = Dart_CObject_kTypedData;
        nonce.value.as_typed_data.type = Dart_TypedData_kUint8;
        nonce.value.as_typed_data.values = nonceValue;
        nonce.value.as_typed_data.length = 4;

        Dart_CObject totalTx;
        totalTx.type = Dart_CObject_kInt32;
        totalTx.value.as_int32 = blocks[i]->totalTx;

        Dart_CObject hashes;

        Dart_CObject hashesCount;
        hashesCount.type = Dart_CObject_kInt64;
        hashesCount.value.as_int64 = blocks[i]->hashesCount;

        Dart_CObject flags;
        //todo

        Dart_CObject flagsLen;
        flagsLen.type = Dart_CObject_kInt64;
        flagsLen.value.as_int64 = blocks[i]->flagsLen;

        Dart_CObject height;
        height.type = Dart_CObject_kInt32;
        height.value.as_int32 = blocks[i]->height;

        Dart_CObject haveData;
        haveData.type = Dart_CObject_kBool;
        haveData.value.as_bool = blocks[i]->haveData;

        Dart_CObject *merkleBlockData[] = {
            &blockHash,
            &version,
            &prevBlock,
            &merkleRoot,
            &timeStamp,
            &target,
            &nonce,
            &totalTx,
            &hashesCount,
            &flagsLen,
            &height,
            &haveData,
        };

        Dart_CObject merkleBlock;
        merkleBlock.type = Dart_CObject_kArray;
        merkleBlock.value.as_array.values = merkleBlockData;
        merkleBlock.value.as_array.length = 12;

        Dart_CObject dartReplace;
        dartReplace.type = Dart_CObject_kBool;
        dartReplace.value.as_bool = replace;

        Dart_CObject *requestData[] = {
            &dartReplace,
            &merkleBlock,
        };

        Dart_CObject data;
        data.type = Dart_CObject_kArray;
        data.value.as_array.values = requestData;
        data.value.as_array.length = 2;

        Dart_CObject *c_request_arr[] = {
            &c_method_name,
            &data,
        };

        Dart_CObject c_request;
        c_request.type = Dart_CObject_kArray;
        c_request.value.as_array.values = c_request_arr;
        c_request.value.as_array.length = 2;

        NotifyDart(peer_send_port, &c_request);
    }
}
void savePeers(void *info, int replace, const Peer peers[], uint64_t peersCount)
{
}
int networkIsReachable(void *info)
{
    return 1;
}

/**
 * Wallet Callbacks
*/

void balanceChanged(void *info, uint64_t balance)
{
    const char *methodname = "balanceChangedDart";
    Dart_CObject c_method_name;
    c_method_name.type = Dart_CObject_kString;
    c_method_name.value.as_string = (char *)(methodname);

    Dart_CObject dartBalance;
    uint8_t *balanceValue = malloc(sizeof(uint64_t));
    memcpy(balanceValue, &balance, 8);
    dartBalance.type = Dart_CObject_kTypedData;
    dartBalance.value.as_typed_data.type = Dart_TypedData_kUint8;
    dartBalance.value.as_typed_data.values = balanceValue;
    dartBalance.value.as_typed_data.length = 8;

    Dart_CObject *requestData[] = {
        &dartBalance,
    };

    Dart_CObject data;
    data.type = Dart_CObject_kArray;
    data.value.as_array.values = requestData;
    data.value.as_array.length = 1;

    Dart_CObject *c_request_arr[] = {
        &c_method_name,
        &data,
    };

    Dart_CObject c_request;
    c_request.type = Dart_CObject_kArray;
    c_request.value.as_array.values = c_request_arr;
    c_request.value.as_array.length = 2;

    NotifyDart(wallet_send_port, &c_request);
}
void walletTxAdded(void *info, Transaction *tx) {}
void txUpdated(void *info, const uint256 txHashes[], uint64_t txCount, uint32_t blockHeight, uint32_t timestamp) {}
void txDeleted(void *info, uint256 txHash, int notifyUser, int recommendRescan) {}

/*
Address functions
*/
LIBAPI int addressIsValid(char *address)
{
    return AddressIsValid(address);
}

/*
Wallet functions
*/
LIBAPI Wallet *createWallet(char *words)
{
    uint512 seed = UINT512_ZERO;
    MasterPubKey mpk = _MASTER_PUBKEY_NONE;
    Wallet *wallet;

    BIP39DeriveKey(seed.data, words, NULL);
    mpk = BIP32MasterPubKey(&seed, sizeof(seed));

    wallet = WalletNew(NULL, 0, &mpk);
    return wallet;
}

LIBAPI void releaseWallet(Wallet *wallet)
{
    WalletFree(wallet);
}

LIBAPI void setWalletCallbacks(Wallet *wallet)
{
    WalletSetCallbacks(wallet, wallet, balanceChanged, walletTxAdded, txUpdated, txDeleted);
}

LIBAPI void receiveAddress(Wallet *wallet, char *address)
{
    memcpy(address, WalletReceiveAddress(wallet).s, strlen(WalletReceiveAddress(wallet).s));
}

LIBAPI int64_t allAddresses(Wallet *wallet, Address *addrs)
{
    return WalletAllAddrs(wallet, addrs, sizeof(*addrs));
}

LIBAPI int containsAddress(Wallet *wallet, char *address)
{
    return WalletContainsAddress(wallet, address);
}

LIBAPI int addressIsUsed(Wallet *wallet, char *address)
{
    return WalletAddressIsUsed(wallet, address);
}

LIBAPI uint64_t transactions(Wallet *wallet, Transaction **transactions, u_int64_t txCount)
{
    return WalletTransactions(wallet, transactions, txCount);
}

LIBAPI uint64_t balance(Wallet *wallet)
{
    return WalletBalance(wallet);
}

LIBAPI uint64_t totalSent(Wallet *wallet)
{
    return WalletTotalSent(wallet);
}

LIBAPI uint64_t feePerKb(Wallet *wallet)
{
    return WalletFeePerKb(wallet);
}

LIBAPI void setFeePerKb(Wallet *wallet, uint64_t value)
{
    WalletSetFeePerKb(wallet, value);
}

LIBAPI uint64_t feeForTx(Wallet *wallet, uint64_t amount)
{
    return WalletFeeForTxAmount(wallet, amount);
}

LIBAPI Transaction *createTransaction(Wallet *wallet, uint64_t forAmount, char *toAddress)
{
    return WalletCreateTransaction(wallet, forAmount, toAddress);
}

LIBAPI Transaction *createTxForOutpust(Wallet *wallet, TxOutput *outputs)
{
    return WalletCreateTxForOutputs(wallet, outputs, sizeof(outputs));
}

LIBAPI int signTransaction(Wallet *wallet, Transaction *tx, int forkId, char *phrase)
{
    uint512 seed = UINT512_ZERO;
    MasterPubKey mpk = _MASTER_PUBKEY_NONE;

    BIP39DeriveKey(seed.data, phrase, NULL);
    return WalletSignTransaction(wallet, tx, forkId, &seed, strlen(phrase));
}

LIBAPI int transactionIsValid(Wallet *wallet, Transaction *tx)
{
    return WalletTransactionIsValid(wallet, tx);
}

LIBAPI int transactionIsPending(Wallet *wallet, Transaction *tx)
{
    return WalletTransactionIsPending(wallet, tx);
}

LIBAPI int transactionIsVerified(Wallet *wallet, Transaction *tx)
{
    return WalletTransactionIsVerified(wallet, tx);
}

LIBAPI uint64_t amountReceivedFromTx(Wallet *wallet, Transaction *tx)
{
    return WalletAmountReceivedFromTx(wallet, tx);
}

LIBAPI uint64_t amountSentByTx(Wallet *wallet, Transaction *tx)
{
    return WalletAmountSentByTx(wallet, tx);
}

LIBAPI uint64_t feeForTransaction(Wallet *wallet, Transaction *tx)
{
    uint64_t fee = WalletFeeForTx(wallet, tx);
    return fee == UINT64_MAX ? 0 : fee;
}

LIBAPI uint64_t balanceAfterTx(Wallet *wallet, Transaction *tx)
{
    return WalletBalanceAfterTx(wallet, tx);
}

LIBAPI uint64_t feeForTxSize(Wallet *wallet, int size)
{
    return WalletFeeForTxSize(wallet, size);
}

LIBAPI uint64_t minOutPutAmount(Wallet *wallet)
{
    return WalletMinOutputAmount(wallet);
}

LIBAPI uint64_t maxOutputAmount(Wallet *wallet)
{
    return WalletMaxOutputAmount(wallet);
}

LIBAPI void walletShutdown(Wallet *wallet, PeerManager *manager)
{
    PeerManagerFree(manager);
    WalletFree(wallet);
}

/*
Peer functions
*/
LIBAPI PeerManager *createNetworkManager(Wallet *wallet, uint32_t earliestKeyTime)
{
    PeerManager *manager;
    manager = PeerManagerNew(wallet, earliestKeyTime, NULL, 0);
    return manager;
}

LIBAPI void releaseNetworkManager(PeerManager *manager)
{
    PeerManagerFree(manager);
}

LIBAPI void setPeerManagerCallbacks(PeerManager *manager)
{
    PeerManagerSetCallbacks(manager, manager, syncStarted, syncStopped, txStatusUpdate, saveHeaders, NULL, NULL, NULL, NULL);
}

LIBAPI int isConnected(PeerManager *manager)
{
    return PeerManagerIsConnected(manager);
}

LIBAPI void peerManagerConnect(PeerManager *manager)
{
    PeerManagerConnect(manager);
}

LIBAPI void peerManagerDisconnect(PeerManager *manager)
{
    PeerManagerDisconnect(peerManager);
}

LIBAPI void rescan(PeerManager *manager)
{
    PeerManagerRescan(manager);
}

LIBAPI uint32_t lastBlockHeight(PeerManager *manager)
{
    return PeerManagerLastBlockHeight(manager);
}

LIBAPI uint32_t lastBlockTimeStamp(PeerManager *manager)
{
    return PeerManagerLastBlockTimestamp(manager);
}

LIBAPI uint32_t lastHeaderHeight(PeerManager *manager)
{
    return PeerManagerLastHeaderHeight(manager);
}

LIBAPI uint32_t estimatedBlockHeight(PeerManager *manager)
{
    return PeerManagerEstimatedBlockHeight(manager);
}

LIBAPI double syncProgress(PeerManager *manager, u_int32_t startHeight)
{
    return PeerManagerSyncProgress(manager, startHeight);
}

LIBAPI uint64_t peerCount(PeerManager *manager)
{
    return PeerManagerPeerCount(manager);
}

LIBAPI char *downloadPeerName(PeerManager *manager)
{
    return PeerManagerDownloadPeerName(manager);
}

LIBAPI void publishTx(PeerManager *manager, Transaction *tx, void *info, void (*callback)(void *info, int error))
{
    PeerManagerPublishTx(manager, tx, info, callback);
}

LIBAPI void setFixedPeer(PeerManager *manager, uint32_t address, uint16_t port)
{
    if (address != 0)
    {
        UInt128 newAddress = UINT128_ZERO;
        newAddress.u16[5] = 0xffff;
        newAddress.u32[3] = address;
        PeerManagerSetFixedPeer(manager, newAddress, port);
    }
    else
    {
        PeerManagerSetFixedPeer(manager, UINT128_ZERO, 0);
    }
}

LIBAPI void addBlockToSet(PeerManager *manager, uint8_t *serializedBlock)
{
    PeerManagerAddBlock(manager, serializedBlock);
}
