
#include "BIP38Key.h"
#include "BIP39Mnemonic.h"
#include "BIP39WordsEn.h"
#include "address.h"
#include "peerManager.h"
#include "wallet.h"
#include "uint512.h"
#include "serialize.h"

// For the typedefs, but don't use the function pointers.
#include "include/dart_api_dl.h"
#include "include/dart_api_dl.c"

#define SKIP_BIP38 1
#define LIBAPI __attribute__((visibility("default")))

#ifdef __ANDROID__
#include <android/log.h>
#define fprintf(...) __android_log_print(ANDROID_LOG_ERROR, "bread", _va_rest(__VA_ARGS__, NULL))
#define printf(...) __android_log_print(ANDROID_LOG_INFO, "bread", __VA_ARGS__)
#define _va_first(first, ...) first
#define _va_rest(first, ...) __VA_ARGS__
#elif __APPLE__
#include <os/log.h>
#include <TargetConditionals.h>
#if TARGET_IPHONE_SIMULATOR && TARGET_OS_IPHONE
// iOS Simulator

fprintf(...) os_log(OS_LOG_DEFAULT, _va_rest(__VA_ARGS__, NULL));
printf(...) os_log(OS_LOG_DEFAULT, __VA_ARGS__);
#define _va_first(first, ...) first
#define _va_rest(first, ...) __VA_ARGS__
#elif TARGET_OS_MAC
// Other kinds of Mac OS
#else
#error "Unknown Apple platform"
#endif
#else
#include <stdio.h>
#define _peer_log(...) printf(__VA_ARGS__)
#endif
/*
Dart native port stuff
*/
LIBAPI intptr_t InitDartApiDL(void *data);
LIBAPI Dart_Port registerPeerDartPort(Dart_Port send_port);
LIBAPI Dart_Port registerRoamDartPort(Dart_Port send_port);
LIBAPI Dart_Port registerWalletDartPort(Dart_Port send_port);
// Dart_CObject buildMerkleBlock(MerkleBlock *block);
/*
Address functions
*/

LIBAPI int addressIsValid(char *address);

/*
Wallet functions

*/
LIBAPI Wallet *createWallet(char *words);

LIBAPI void releaseWallet(Wallet *wallet);

LIBAPI void setWalletCallbacks(Wallet *wallet);

LIBAPI void receiveAddress(Wallet *wallet, char *address);

LIBAPI int64_t allAddresses(Wallet *wallet, Address *addrs);

LIBAPI int containsAddress(Wallet *wallet, char *address);

LIBAPI int addressIsUsed(Wallet *wallet, char *address);

LIBAPI uint64_t transactions(Wallet *wallet, Transaction **transactions, u_int64_t txCount);

LIBAPI uint64_t balance(Wallet *wallet);

LIBAPI uint64_t totalSent(Wallet *wallet);

LIBAPI uint64_t feePerKb(Wallet *wallet);

LIBAPI uint64_t feeForTx(Wallet *wallet, uint64_t amount);

LIBAPI void setFeePerKb(Wallet *wallet, uint64_t value);

LIBAPI Transaction *createTransaction(Wallet *wallet, uint64_t forAmount, char *toAddress);

LIBAPI Transaction *createTxForOutpust(Wallet *wallet, TxOutput *outputs);

LIBAPI int signTransaction(Wallet *wallet, Transaction *tx, int forkId, char *phrase);

LIBAPI int transactionIsValid(Wallet *wallet, Transaction *tx);

LIBAPI int transactionIsPending(Wallet *wallet, Transaction *tx);

LIBAPI int transactionIsVerified(Wallet *wallet, Transaction *tx);

LIBAPI uint64_t amountReceivedFromTx(Wallet *wallet, Transaction *tx);

LIBAPI uint64_t amountSentByTx(Wallet *wallet, Transaction *tx);

LIBAPI uint64_t feeForTransaction(Wallet *wallet, Transaction *tx);

LIBAPI uint64_t balanceAfterTx(Wallet *wallet, Transaction *tx);

LIBAPI uint64_t feeForTxSize(Wallet *wallet, int size);

LIBAPI uint64_t minOutPutAmount(Wallet *wallet);

LIBAPI uint64_t maxOutputAmount(Wallet *wallet);

LIBAPI void walletShutdown(Wallet *wallet, PeerManager *manager);

/*
Peer functions

*/
LIBAPI PeerManager *createNetworkManager(Wallet *wallet, uint32_t earliestKeyTime);

LIBAPI void releaseNetworkManager(PeerManager *manager);

LIBAPI void setPeerManagerCallbacks(PeerManager *manager);

LIBAPI int isConnected(PeerManager *manager);

LIBAPI void peerManagerConnect(PeerManager *manager);

LIBAPI void peerManagerDisconnect(PeerManager *manager);

LIBAPI void rescan(PeerManager *manager);
LIBAPI uint32_t lastBlockHeight(PeerManager *manager);
LIBAPI uint32_t lastBlockTimeStamp(PeerManager *manager);
LIBAPI uint32_t lastHeaderHeight(PeerManager *manager);
LIBAPI uint32_t estimatedBlockHeight(PeerManager *manager);
LIBAPI double syncProgress(PeerManager *manager, u_int32_t startHeight);
LIBAPI uint64_t peerCount(PeerManager *manager);
LIBAPI char *downloadPeerName(PeerManager *manager);
LIBAPI void publishTx(PeerManager *manager, Transaction *tx, void *info, void (*callback)(void *info, int error));
LIBAPI void setFixedPeer(PeerManager *manager, uint32_t address, uint16_t port);
LIBAPI void addBlockToSet(PeerManager *manager, uint8_t *serializedBlock);
